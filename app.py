from gevent import monkey
monkey.patch_all()

import time
from threading import Thread
from flask import Flask, render_template, session, request
from flask.ext.socketio import SocketIO, emit, join_room, leave_room, \
    close_room, disconnect

import logging
from logging.handlers import RotatingFileHandler


UP = 38
DOWN = 40


app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
thread = None


def background_thread():
    """Example of how to send server generated events to clients."""
    count = 0
    while True:
        time.sleep(10)
        count += 1
        socketio.emit('my response',
                      {'data': 'allo from server', 'count': count},
                      namespace='/test')


@app.route('/')
def index():
    global thread
    if thread is None:
        thread = Thread(target=background_thread)
        thread.start()
    return render_template('index.html')



@socketio.on('direction', namespace='/test')
def direction(direction):
    app.logger.debug(direction)
    
    session['receive_count'] = session.get('receive_count', 0) + 1
    
    msg = go_direction(direction)
    
    emit('my response',
         {'data': msg, 'count': session['receive_count']})


def go_direction(direction):
    if (direction['data'] == UP):
        return 'UP'
    elif (direction['data'] == DOWN):
        return 'DOWN'
    else:
        return 'unknown code'
    time.sleep(0.2)
    #met a 0 sur 



@socketio.on('disconnect request', namespace='/test')
def disconnect_request():
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my response',
         {'data': 'Disconnected!', 'count': session['receive_count']})
    disconnect()


@socketio.on('connect', namespace='/test')
def test_connect():
    emit('my response', {'data': 'Connected', 'count': 0})


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')



if __name__ == '__main__':
    handler = RotatingFileHandler('foo.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.DEBUG)
    app.logger.addHandler(handler)
    socketio.run(app,host="0.0.0.0")
